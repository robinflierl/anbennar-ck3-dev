﻿### General ###
namespace = anb_spell_interruption

# Out of Range #
anb_spell_interruption.1 = {
	type = character_event
	title = anb_spell_interruption.1.t
	desc = anb_spell_interruption.1.d

	theme = witchcraft
	left_portrait = {
		character = scope:owner
		animation = idle
	}
	right_portrait = {
		character = scope:target
		animation = idle
	}
	
	option = {
		name = anb_spell_interruption.1.a
	}
}

anb_spell_interruption.101 = {
	type = character_event
	title = anb_spell_interruption.1.t
	desc = anb_spell_interruption.101.d

	theme = witchcraft
	left_portrait = {
		character = scope:owner
		animation = idle
	}
	right_portrait = {
		character = scope:target
		animation = idle
	}
	
	option = {
		name = anb_spell_interruption.101.a
	}
}	
	
# Imprisoned #
anb_spell_interruption.2 = {
	type = character_event
	title = anb_spell_interruption.1.t
	desc = anb_spell_interruption.2.d

	theme = prison
	left_portrait = {
		character = scope:owner
		animation = disbelief
	}
	right_portrait = {
		character = scope:target
		animation = prison_dungeon
	}
	
	option = {
		name = anb_spell_interruption.2.a
	}	
}

# Target Died #
anb_spell_interruption.3 = {
	type = character_event
	title = anb_spell_interruption.1.t
	desc = anb_spell_interruption.3.d

	theme = death
	left_portrait = {
		character = scope:owner
		animation = disbelief
	}
	right_portrait = {
		character = scope:target
		animation = idle
	}
	
	option = {
		name = anb_spell_interruption.3.a
	}	
}

# Target Incapable #
anb_spell_interruption.4 = {
	type = character_event
	title = anb_spell_interruption.1.t
	desc = anb_spell_interruption.4.d

	theme = witchcraft
	left_portrait = {
		character = scope:owner
		animation = disbelief
	}
	right_portrait = {
		character = scope:target
		animation = idle
	}
	
	option = {
		name = anb_spell_interruption.4.a
	}	
}

# Spell already Active #
anb_spell_interruption.5 = {
	type = character_event
	title = anb_spell_interruption.1.t
	desc = anb_spell_interruption.5.d
	orphan = yes
	theme = witchcraft
	left_portrait = {
		character = scope:owner
		animation = disbelief
	}
	right_portrait = {
		character = scope:target
		animation = idle
	}
	
	option = {
		name = anb_spell_interruption.5.a
	}	
}

# Hostile Relations #
anb_spell_interruption.6 = {
	type = character_event
	title = anb_spell_interruption.1.t
	desc = anb_spell_interruption.6.d

	theme = witchcraft
	left_portrait = {
		character = scope:owner
		animation = rage
	}
	right_portrait = {
		character = scope:target
		animation = rage
	}
	
	option = {
		name = anb_spell_interruption.6.a
	}	
}

anb_spell_interruption.106 = {
	type = character_event
	title = anb_spell_interruption.1.t
	desc = anb_spell_interruption.106.d

	theme = witchcraft
	left_portrait = {
		character = scope:owner
		animation = rage
	}
	right_portrait = {
		character = scope:target
		animation = rage
	}
	
	option = {
		name = anb_spell_interruption.106.a
	}	
}

### Compel & Dominate ###

# Already has Weak Hook #
anb_spell_interruption.10 = {
	type = character_event
	title = anb_spell_interruption.1.t
	desc = anb_spell_interruption.10.d

	theme = witchcraft
	left_portrait = {
		character = scope:owner
		animation = scheme
	}
	right_portrait = {
		character = scope:target
		animation = idle
	}
	
	option = {
		name = anb_spell_interruption.10.a
	}	
}

# Already has Strong Hook #
anb_spell_interruption.11 = {
	type = character_event
	title = anb_spell_interruption.1.t
	desc = anb_spell_interruption.11.d

	theme = witchcraft
	left_portrait = {
		character = scope:owner
		animation = scheme
	}
	right_portrait = {
		character = scope:target
		animation = idle
	}
	
	option = {
		name = anb_spell_interruption.11.a
	}	
}