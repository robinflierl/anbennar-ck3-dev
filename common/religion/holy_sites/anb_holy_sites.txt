﻿###NOTE - Place holy sites that use same location next to each other for easy comparison
##Regent Court #USAGE - in cannor+used by regent court faiths, holy sites outside of cannor should only go here if they are exclusvely used by regent court faiths
moonmount = {
	county = c_moonmount
	barony = b_temple_of_the_highest_moon
	
	character_modifier = {
		name = holy_site_moonmount_effect_name
		monthly_learning_lifestyle_xp_gain_mult = 0.15
	}
}

port_munas = {
	county = c_moonhaven
	barony = b_port_munas
	
	character_modifier = {
		name = holy_site_port_munas_effect_name
		naval_movement_speed_mult = 0.25
		embarkation_cost_mult = -0.25
	}
}

windtower = {
	county = c_dockbridge
	barony = b_windtower
	
	character_modifier = {
		name = holy_site_windtower_effect_name
	}
}

silverspire = {
	county = c_silvelar
	barony = b_silverspire
	
	character_modifier = {
		name = holy_site_silverspire_effect_name
	}
}

mountains_of_the_moon = {
	county = c_sad_kuz
	barony = b_sad_kuz
	
	character_modifier = {
		name = holy_site_mountains_of_the_moon_effect_name
	}
}

lorentaine = {
	county = c_lorentaine
	barony = b_highcour
	
	character_modifier = {
		name = holy_site_lorentaine_effect_name
		prowess_per_piety_level = 3
	}
}

minar = {
	county = c_minar
	barony = b_minaras_bosom
	
	character_modifier = {
		name = holy_site_minar_effect_name
		stress_loss_mult = 0.1
		fertility = 0.05
	}
}

horsegarden = {
	county = c_horsegarden
	barony = b_horsegarden
	
	character_modifier = {
		name = holy_site_horsegarden_effect_name
		heavy_cavalry_max_size_add = 2
	}
}

taranton = {
	county = c_taranton
	barony = b_taranton
	
	character_modifier = {
		name = holy_site_taranton_effect_name
		character_travel_speed = 15
		monthly_prestige_gain_per_knight_add = 0.5
	}
}

ryalanar = {
	county = c_ryalanar
	barony = b_ryalanar
	
	character_modifier = {
		name = holy_site_ryalanar_effect_name
		general_opinion = 10
	}
}

north_citadel = {
	county = c_the_north_citadel
	barony = b_the_north_citadel
	
	character_modifier = {
		name = holy_site_north_citadel_effect_name
		direct_vassal_opinion = 10
	}
}

trialmount = {
	county = c_the_north_citadel
	barony = b_the_north_citadel
	
	character_modifier = {
		name = holy_site_trialmount_effect_name
		legitimacy_gain_mult = 0.1
		monthly_lifestyle_xp_gain_mult = 0.05
	}
}

the_pantheonway = {
	county = c_north_castonath
	barony = b_pantheonway
	
	flag = pantheonway_conversion_bonus # +20% county conversion speed
}

vertesk = {
	county = c_vertesk
	barony = b_vertesk
	
	character_modifier = {
		name = holy_site_vertesk_effect_name
		owned_hostile_scheme_success_chance_add = 0.1
	}
}

bal_vertesk = {
	county = c_vertesk
	barony = b_vertesk
	
	character_modifier = {
		name = holy_site_bal_vertesk_effect_name
		owned_hostile_scheme_success_chance_add = 0.1
	}
}

xhazobains_end = {
	county = c_xhazobain_end
	barony = b_bronhyl
	
	character_modifier = {
		name = holy_site_xhazobains_end_effect_name
		martial_per_piety_level = 2
	}
}

bal_vroren = {
	county = c_bal_vroren
	barony = b_bal_vroren
	
	character_modifier = {
		name = holy_site_bal_vroren_effect_name
		prowess_per_piety_level = 1
		supply_duration = 0.2
	}
}

adeanscour = {
	county = c_new_adea
	barony = b_adeanscour
	
	character_modifier = {
		name = holy_site_adeanscour_effect_name
		prowess_per_piety_level = 1
		advantage_against_coreligionists = 5
	}
}

golden_plains = {
	county = c_golden_plains
	barony = b_golden_plains
	
	character_modifier = {
		name = holy_site_golden_plains_effect_name
	}
}

lancevalley = {
	county = c_lasean
	barony = b_lasean
	
	character_modifier = {
		name = holy_site_lancevalley_effect_name
	}
}

toarnaire = {
	county = c_saloren
	barony = b_toarnaire
	
	character_modifier = {
		name = holy_site_toarnaire_effect_name
		development_growth_factor = 0.1
		diplomacy = 1
	}
}

portnamm = {
	county = c_portnamm
	barony = b_portnamm
	
	character_modifier = {
		name = holy_site_portnamm_effect_name
		learning_per_piety_level = 3
	}
}

nimsnoms = {
	county = c_portnamm
	barony = b_nimsnoms
	
	character_modifier = {
		name = holy_site_nimsnoms_effect_name
		clergy_opinion = 5
	}
}

esmaraine = {
	county = c_esmaraine
	barony = b_esmaraine
	
	character_modifier = {
		name = holy_site_esmaraine_effect_name
		stewardship_per_piety_level = 1
		fertility = 0.1
	}
}

mothers_sanctuary = {
	county = c_esmaraine
	barony = b_holy_hearth
	
	character_modifier = {
		name = holy_site_mothers_sanctuary_effect_name
		stewardship_per_piety_level = 1
		fertility = 0.1
	}
}

aramar = {
	county = c_aramar
	barony = b_aramar
	
	character_modifier = {
		name = holy_site_aramar_effect_name
		monthly_stewardship_lifestyle_xp_gain_mult = 0.15
	}
}

barrowshire = {
	county = c_barrowshire
	barony = b_barrowburgh
	
	character_modifier = {
		name = holy_site_barrowshire_effect_name
		development_growth_factor = 0.1
	}
}

beepeck = {
	county = c_beepeck
	barony = b_beepeck
	
	character_modifier = {
		name = holy_site_beepeck_effect_name
		monthly_diplomacy_lifestyle_xp_gain_mult = 0.15
	}
}

beepeck_begga = {
	county = c_beepeck
	barony = b_beepeck
	
	character_modifier = {
		name = holy_site_beepeck_effect_name
		monthly_diplomacy_lifestyle_xp_gain_mult = 0.15
	}
}

varaine = {
	county = c_varaine
	barony = b_varaine
	
	character_modifier = {
		name = holy_site_varaine_effect_name
		negate_health_penalty_add = 1
	}
}

gaweton = {
	county = c_gaweton
	barony = b_gaweton
	
	character_modifier = {
		name = holy_site_gaweton_effect_name
		dynasty_opinion = 10
	}
}

bardswood = {
	county = c_bardswood
	barony = b_bardswood
	
	character_modifier = {
		name = holy_site_bardswood_effect_name
		attraction_opinion = 10
	}
}

seinathil = {
	county = c_seinathil
	barony = b_seinathil
	
	character_modifier = {
		name = holy_site_seinathil_effect_name
		monthly_diplomacy_lifestyle_xp_gain_mult = 0.15
	}
}

lovers_quarrel = {
	county = c_lovers_quarrel
	barony = b_lovers_quarrel
	
	character_modifier = {
		name = holy_site_lovers_quarrel_effect_name
		hostile_scheme_phase_duration_add = -10
	}
}

anbenncost = {
	county = c_anbenncost
	barony = b_temple_district
	
	character_modifier = {
		name = holy_site_anbenncost_effect_name
		development_growth_factor = 0.1
		diplomacy = 1
	}
}

bayvic = {
	county = c_bayvic
	barony = b_bayvic
	
	character_modifier = {
		name = holy_site_bayvic_effect_name
	}
}

toref = {
	county = c_toref_citadel
	barony = b_toref_citadel
	
	character_modifier = {
		name = holy_site_toref_effect_name
	}
}

silverforge = {
	county = c_silverforge
	barony = b_dorum_odir
	
	character_modifier = {
		name = holy_site_silverforge_effect_name
		monthly_piety_from_buildings_mult = 0.5
	}
}

bal_hyl = {
	county = c_wexkeep
	barony = b_wexkeep
	
	character_modifier = {
		name = holy_site_bal_hyl_effect_name
	}
}

dragonforge = {
	county = c_south_castonath
	barony = b_dragonforge_hill
	
	character_modifier = {
		name = holy_site_dragonforge_effect_name
		stewardship_per_piety_level = 1
	}
}

bal_mire = {
	county = c_bal_mire
	barony = b_bal_mire
	
	character_modifier = {
		name = holy_site_bal_mire_effect_name
	}
}

bal_ouord = {
	county = c_bal_ouord
	barony = b_bal_ouord
	
	character_modifier = {
		name = holy_site_bal_ouord_effect_name
	}
}

arca_corvur = {
	county = c_arca_corvur
	barony = b_arca_corvur
	
	character_modifier = {
		name = holy_site_arca_corvur_effect_name
		county_opinion_add = 10
		monthly_prestige_gain_mult = 0.1
	}
}

bal_dostan = {
	county = c_arca_corvur
	barony = b_arca_corvur
	
	character_modifier = {
		#Should be identical to arca_corvur
		name = holy_site_bal_dostan_effect_name
		county_opinion_add = 10
		monthly_prestige_gain_mult = 0.1
	}
}

humacs_rest = {
	county = c_humacs_rest
	barony = b_humacs_rest
	
	character_modifier = {
		name = holy_site_humacs_rest_effect_name
	}
}

the_necropolis = {
	county = c_corseton
	barony = b_the_necropolis

	character_modifier = {
		name = holy_site_the_necropolis_effect_name
		learning_per_piety_level = 1
	}
}

wisphollow = {
	county = c_wisphollow
	barony = b_wisphollow
	
	character_modifier = {
		name = holy_site_wisphollow_effect_name
	}
}

lioncost = {
	county = c_lioncost
	barony = b_lioncost
	
	character_modifier = {
		name = holy_site_lioncost_effect_name
	}
}

nathalaire = {
	county = c_nathalaire
	barony = b_nathalaire
	
	character_modifier = {
		name = holy_site_nathalaire_effect_name
		intrigue_per_piety_level = 1
	}
}

nathalaire_nathalyne = {
	county = c_nathalaire
	barony = b_nathalaire
	
	character_modifier = {
		name = holy_site_nathalaire_effect_name
		intrigue_per_piety_level = 1
	}
}

aldtempel = {
	county = c_aldtempel
	barony = b_aldtempel
	
	character_modifier = {
		name = holy_site_aldtempel_effect_name
		forest_max_combat_roll = 5
		archers_toughness_mult = 0.1
	}
}

elkwood = {
	county = c_elkwood
	barony = b_elkwood
	
	character_modifier = {
		name = holy_site_elkwood_effect_name
	}
}

eyegard = {
	county = c_eyegard
	barony = b_cresthill
	
	character_modifier = {
		name = holy_site_eyegard_effect_name
	}
}

mammoth_hall = {
	county = c_mammoth_hall
	barony = b_mammoth_hall
	
	character_modifier = {
		name = holy_site_mammoth_hall_effect_name
	}
}

houndsmoor = {
	county = c_houndsmoor
	barony = b_test_1457
	
	character_modifier = {
		name = holy_site_houndsmoor_effect_name
	}
}

erngrove = {
	county = c_pearlsedge
	barony = b_erngrove
	
	character_modifier = {
		name = holy_site_erngrove_effect_name
	}
}

westport = {
	county = c_westport
	barony = b_westport
	
	character_modifier = {
		name = holy_site_westport_effect_name
		martial_per_piety_level = 1
		raid_speed = 0.1
	}
}

menibor = {
	county = c_menibor
	barony = b_menibor
	
	character_modifier = {
		name = holy_site_menibor_effect_name
	}
}

bladeskeep = {
	county = c_bladeskeep
	barony = b_bladeskeep
	
	character_modifier = {
		name = holy_site_bladeskeep_effect_name
	}
}

varstone = {
	county = c_bronzewing
	barony = b_varstone
	
	character_modifier = {
		name = holy_site_varstone_effect_name
	}
}

wightsgate = {
	county = c_wightsgate
	barony = b_wightsgate
	
	character_modifier = {
		name = holy_site_wightsgate_effect_name
	}
}

cannwic = {
	county = c_cannwic
	barony = b_cannwic
	
	character_modifier = {
		name = holy_site_cannwic_effect_name
	}
}

varlosen = {
	county = c_varlosen
	barony = b_varlosen
	
	character_modifier = {
		name = holy_site_varlosen_effect_name
		life_expectancy = 5
	}
}

baycodds = {
	county = c_baycodds
	barony = b_baycodds
	
	character_modifier = {
		name = holy_site_baycodds_effect_name
		development_growth_factor = 0.15
	}
}

rosefield = {
	county = c_rosefield
	barony = b_red_reach
	
	character_modifier = {
		name = holy_site_rosefield_effect_name
		light_cavalry_max_size_add = 1
		heavy_cavalry_max_size_add = 1
	}
}

oddansbay = {
	county = c_oddansbay
	barony = b_oddansbay
	
	character_modifier = {
		name = holy_site_oddansbay_effect_name
		knight_effectiveness_mult = 0.2
		knight_limit = 1
	}
}

drekiriki = {
	county = c_oddansbay
	barony = b_oddansbay
	
	character_modifier = {
		name = holy_site_drekiriki_effect_name
		knight_effectiveness_mult = 0.2
		knight_limit = 1
	}
}

reavers_landing = {
	county = c_reavers_landing
	barony = b_reavers_landing
	
	character_modifier = {
		name = holy_site_reavers_landing_effect_name
	}
}

uelacen = {
	county = c_uelaire
	barony = b_uelaire
	
	character_modifier = {
		name = holy_site_uelaire_effect_name
	}
}

venail = {
	county = c_venail
	barony = b_venail
	
	character_modifier = {
		name = holy_site_venail_effect_name
		naval_movement_speed_mult = 0.25
		embarkation_cost_mult = -0.25
	}
}

bhetu = {
	county = c_bhetu
	barony = b_bhetu
	
	character_modifier = {
		name = holy_site_bhetu_effect_name
	}
}

dalostor = {
	county = c_eborthil
	barony = b_dalostor
	
	character_modifier = {
		name = holy_site_dalostor_effect_name
	}
}

port_jaher = {
	county = c_port_jaher
	barony = b_port_jaher
	
	character_modifier = {
		name = holy_site_port_jaher_effect_name
	}
}

##Infernal Cults

varillen = {
	county = c_varillen
	barony = b_varillen
	
	character_modifier = {
		name = holy_site_varillen_effect_name
	}
}

##Kheteratan
kheterat = {
	county = c_kheterat
	barony = b_kheterat
	
	character_modifier = {
		name = holy_site_kheterat_effect_name
		build_gold_cost = -0.2
		monthly_dynasty_prestige_mult = 0.1
	}
}

nirat = {
	county = c_nirat
	barony = b_nirat
	
	character_modifier = {
		name = holy_site_nirat_effect_name
		development_growth_factor = 0.15
	}
}

ibtat = {
	county = c_ibtat
	barony = b_ibtat
	
	character_modifier = {
		name = holy_site_ibtat_effect_name
		stress_loss_mult = 0.25
		councillor_opinion = 5
	}
}

golkora = {
	county = c_golkora
	barony = b_golkora
	
	character_modifier = {
		name = holy_site_golkora_effect_name
		stewardship_per_piety_level = 1
	}
}

##Skaldhyrric
skaldol = {
	county = c_skaldol
	barony = b_skaldol
	
	character_modifier = {
		name = holy_site_skaldol_effect_name
		learning_per_piety_level = 1
	}
}

algrar = {
	county = c_algrar
	barony = b_algrar
	
	character_modifier = {
		name = holy_site_algrar_effect_name
		men_at_arms_maintenance = -0.05
		clergy_opinion = 5
	}
}

coldmarket = {
	county = c_coldmarket
	barony = b_coldmarket
	
	character_modifier = {
		name = holy_site_coldmarket_effect_name
		monthly_piety_gain_mult = 0.1
	}
}

jotunhamr = {
	county = c_jotunhamr
	barony = b_jotunhamr
	
	character_modifier = {
		name = holy_site_jotunhamr_effect_name
		naval_movement_speed_mult = 0.25
		embarkation_cost_mult = -0.25
	}
}

##Elven
silent_repose = {
	county = c_havoral_peak
	barony = b_havoral_peak
	
	character_modifier = {
		name = holy_site_silent_repose_effect_name
		stress_loss_mult = 0.25
		councillor_opinion = 5
	}
}

celmaldor = {
	county = c_celmaldor
	barony = b_celmaldor
	
	character_modifier = {
		name = holy_site_celmaldor_effect_name
		stewardship_per_piety_level = 1
	}
}

moonhaven = {
	county = c_moonhaven
	barony = b_moonhaven
	
	character_modifier = {
		name = holy_site_moonhaven_effect_name
		county_opinion_add = 5
	}
}

##Tanibic
sirik_peak = {
	county = c_sirik
	barony = b_sirik
	
	character_modifier = {
		name = holy_site_sirik_peak_effect_name
		learning = 2
	}
}

hapak_peak = {
	county = c_hapak
	barony = b_hapak
	
	character_modifier = {
		name = holy_site_hapak_peak_effect_name
		mountains_development_growth_factor = 0.2
		mountains_advantage = 5
	}
}

##Pre-Castanorian
enteben = {
	county = c_enteben
	barony = b_enteben
	
	character_modifier = {
		name = holy_site_enteben_effect_name
		prowess_per_piety_level = 2
		martial = 1
	}
}

rewanfork = {
	county = c_rewanfork
	barony = b_rewanfork
	
	character_modifier = {
		name = holy_site_rewanfork_effect_name
		development_growth_factor = 0.15
	}
}

lencesk = {
	county = c_lencesk
	barony = b_merisseidd
	
	character_modifier = {
		name = holy_site_lencesk_effect_name
		character_travel_speed_mult = 0.15
		county_opinion_add = 10
	}
}

the_approach = {
	county = c_the_approach
	barony = b_aldegarde
	
	character_modifier = {
		name = holy_site_the_approach_effect_name
		light_cavalry_max_size_add = 1
		heavy_cavalry_max_size_add = 1
	}
}

carneter = {
	county = c_carneter
	barony = b_carneter
	
	character_modifier = {
		name = holy_site_carneter_effect_name
	}
}

ilvandet = {
	county = c_ilvandet
	barony = b_ilvandet
	
	character_modifier = {
		name = holy_site_ilvandet_effect_name
	}
}

adarvreil = {
	county = c_ioriellen
	barony = b_adarvreil
	
	character_modifier = {
		name = holy_site_adarvreil_effect_name
	}
}

oldhaven = {
	county = c_oldhaven
	barony = b_oldhaven
	
	character_modifier = {
		name = holy_site_oldhaven_effect_name
	}
}

steelhyl = {
	county = c_steelhyl
	barony = b_steelhyl
	
	character_modifier = {
		name = holy_site_steelhyl_effect_name
	}
}

wispmire = {
	county = c_wispmire
	barony = b_wispmire
	
	character_modifier = {
		name = holy_site_wispmire_effect_name
	}
}

##Sun Cult
brasan = {
	county = c_brasan
	barony = b_eduz_betdaris
	
	character_modifier = {
		name = holy_site_brasan_effect_name
	}
}

bulwar = {
	county = c_bulwar
	barony = b_eduz_rakadags
	
	character_modifier = {
		name = holy_site_bulwar_effect_name
		# Temp
	}
}

azkaszelazka = {
	county = c_azkaszelazka
	barony = b_azkaszelazka
	
	character_modifier = {
		name = holy_site_azkaszelazka_effect_name
		# Temp
	}
}

eduz_vacyn = {
	county = c_eduz_vacyn
	barony = b_eduz_vacyn
	
	character_modifier = {
		name = holy_site_eduz_vacyn_effect_name
		# Temp
	}
}

azka_sur = {
	county = c_azka_sur
	barony = b_azka_sur
	
	character_modifier = {
		name = holy_site_azka_sur_effect_name
		# Temp
	}
}

azka_szel_udam = {
	county = c_azka_szel_udam
	barony = b_azka_szel_udam
	
	character_modifier = {
		name = holy_site_azka_szel_udam_effect_name
		# Temp
	}
}

puhiya = {
	county = c_annail
	barony = b_puhiya
	
	character_modifier = {
		name = holy_site_puhiya_effect_name
		# Temp
	}
}

aqatbar = {
	county = c_aqatbar
	barony = b_eduz_ginakku
	
	character_modifier = {
		name = holy_site_aqatbar_effect_name
		# Temp
	}
}

eduz_szel_ninezim = {
	county = c_eduz_szel_ninezim
	barony = b_eduz_szel_ninezim
	
	character_modifier = {
		name = holy_site_eduz_szel_ninezim_effect_name
		# Temp
	}
}

tazh_qel_araksa = {
	county = c_akalses
	barony = b_tazh_qel_araksa
	
	character_modifier = {
		name = holy_site_tazh_qel_araksa_effect_name
		# Temp
	}
}

azkabar = {
	county = c_azkabar
	barony = b_azkabar
	
	character_modifier = {
		name = holy_site_azkabar_effect_name
		# Temp
	}
}

eduz_satkuza = {
	county = c_kuztanuz
	barony = b_eduz_satkuza
	
	character_modifier = {
		name = holy_site_eduz_satkuza_effect_name
		# Temp
	}
}

ekluzagnu = {
	county = c_ekluzagnu
	barony = b_ekluzagnu
	
	character_modifier = {
		name = holy_site_ekluzagnu_effect_name
		# Temp
	}
}

mount_lazzaward = {
	county = c_lawassar
	barony = b_lawassar
	
	character_modifier = {
		name = holy_site_mount_lazzaward_effect_name
		# Temp
	}
}

ebbusubtu = {
	county = c_ebbusubtu
	barony = b_ebbusubtu
	
	character_modifier = {
		name = holy_site_ebbusubtu_effect_name
		# Temp
	}
}

birsartansbar = {
	county = c_birsartansbar
	barony = b_birsartansbar
	
	character_modifier = {
		name = holy_site_birsartansbar_effect_name
		# Temp
	}
}

heunthume = {
	county = c_heunthume
	barony = b_heunthume
	
	character_modifier = {
		name = holy_site_heunthume_effect_name
		# Temp
	}
}

firanyalen = {
	county = c_firanyalen
	barony = b_firanyalen
	
	character_modifier = {
		name = holy_site_firanyalen_effect_name
		# Temp
	}
}

siadunaiun = {
	county = c_siadunaiun
	barony = b_siadunaiun
	
	character_modifier = {
		name = holy_site_siadunaiun_effect_name
		# Temp
	}
}

misarallen = {
	county = c_misarallen
	barony = b_misarallen
	
	character_modifier = {
		name = holy_site_misarallen_effect_name
		# Temp
	}
}

duklum_tanuz = {
	county = c_duklum_tanuz
	barony = b_jaharran
	
	character_modifier = {
		name = holy_site_duklum_tanuz_effect_name
		# Temp
	}
}

fatherwell = {
	county = c_fatherwell
	barony = b_fatherwell
	
	character_modifier = {
		name = holy_site_fatherwell_effect_name
		# Temp
	}
}

edesukeru = {
	county = c_edesukeru
	barony = b_edesukeru
	
	character_modifier = {
		name = holy_site_edesukeru_effect_name
		# Temp
	}
}

##The Thought

coddorran = {
	county = c_coddorran
	barony = b_coddorran
	
	character_modifier = {
		name = holy_site_coddorran_effect_name
		stewardship_per_piety_level = 1
	}
}

##Dragon Cults
karns_hold = {
	county = c_karns_hold
	barony = b_codeseimul
	
	character_modifier = {
		name = holy_site_karns_hold_effect_name
		defender_advantage = 5	
	}
}

banesfork = {
	county = c_banesfork
	barony = b_banesfork
	
	character_modifier = {
		name = holy_site_banesfork_effect_name
		character_travel_speed = 15
		supply_duration = 0.25
	}
}

ravenhill = {
	county = c_ravenhill
	barony = b_ravenhill
	
	character_modifier = {
		name = holy_site_ravenhill_effect_name
		martial_per_piety_level = 1
		stewardship_per_piety_level = 1
	}
}

corvelds_coast = {
	county = c_corveld_coast
	barony = b_jorathur
	
	character_modifier = {
		name = holy_site_corvelds_coast_effect_name
		monthly_piety = 0.1
	}
}

damescrown = {
	county = c_damescrown
	barony = b_damescrown
	
	character_modifier = {
		name = holy_site_damescrown_effect_name
	}
}

old_damenath = {
	county = c_old_damenath
	barony = b_old_damenath
	
	character_modifier = {
		name = holy_site_old_damenath_effect_name
	}
}

redfort = {
	county = c_redfort
	barony = b_redfort
	
	character_modifier = {
		name = holy_site_redfort_effect_name
	}
}

westgate = {
	county = c_westgate
	barony = b_westgate
	
	character_modifier = {
		name = holy_site_old_westgate_effect_name
	}
}

soxun_kobildzex = {
	county = c_soxun_kobildzex
	barony = b_soxun_kobildzex
	
	character_modifier = {
		name = holy_site_soxun_kobildzex_effect_name
	}
}

fetaginbaia = {
	county = c_oddansbay
	barony = b_oddansbay
	
	character_modifier = {
		name = holy_site_fetaginbaia_effect_name
	}
}

mawdock = {
	county = c_mawdock
	barony = b_mawdock
	
	character_modifier = {
		name = holy_site_mawdock_effect_name
	}
}

ayddexahea = {
	county = c_royoddan
	barony = b_royoddan
	
	character_modifier = {
		name = holy_site_ayddexahea_effect_name
	}
}

#Temporary Dwarf holy sites
rubyhold = {
	county = c_rubyhold
	barony = b_hakuzal_hargun
	
	character_modifier = {
		name = holy_site_rubyhold_effect_name
	}
}

khugdihr = {
	county = c_khugdihr
	barony = b_khugdihr
	
	character_modifier = {
		name = holy_site_khugdihr_effect_name
	}
}

ovdal_tungr = {
	county = c_ovdal_tungr
	barony = b_attabur_gorodr
	
	character_modifier = {
		name = holy_site_ovdal_tungr_effect_name
	}
}
seghdihr = {
	county = c_seghdihr
	barony = b_ubrdwar_tygr
	
	character_modifier = {
		name = holy_site_seghdihr_effect_name
	}
}
verkal_gulan = {
	county = c_verkal_gulan
	barony = b_kar_dewarov
	
	character_modifier = {
		name = holy_site_verkal_gulan_effect_name
	}
}
