﻿on_100th_birthday = {
	trigger = { age = 100 }

	events = {
		anb_coming_of_age.0001
	}

}

on_300th_birthday = {
	trigger = { age = 300 }

	events = {
		anb_coming_of_age.0002
	}

}