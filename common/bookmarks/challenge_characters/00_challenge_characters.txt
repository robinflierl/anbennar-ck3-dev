﻿# Anbennar Modified

challenge_bookmark_test_marion_silmuna = {
	start_date = 1022.1.1
	character = {
		name = "bookmark_test_marion_silmuna"
		dynasty = dynasty_silmuna
		dynasty_splendor_level = 1
		type = male
		birth = 1001.2.19
		title = k_dameria
		government = feudal_government
		culture = damerian
		religion = cult_of_the_dame
		difficulty = "BOOKMARK_CHARACTER_DIFFICULTY_EASY"
		history_id = silmuna_0001

		animation = disapproval
	}
}