d_avamezan = {
	1011.3.4 = {
		holder = bulwari0011 #Radin szel-Ĕrubas
	}
}

c_nerdu_avamezan = {
	1000.1.1 = { change_development_level = 14 }
}

c_kutiriq = {
	1000.1.1 = { change_development_level = 12 }
}

c_erubas = {
	1000.1.1 = { change_development_level = 16 }
	1011.3.4 = {
		holder = bulwari0011 #Radin szel-Ĕrubas
	}
}

c_dazar_avamezan = {
	1000.1.1 = { change_development_level = 16 }
}