k_akalses = {
	1007.12.9 = {
		holder = araskaysit0001 #Dagan szel-Akalšes
	}
}

d_azkabar = {
	1006.6.22 = {
		liege = e_bulwar
		holder = sun_elvish0006	# Eledas I Sarelzuir
	}
}

c_akalses = {
	1000.1.1 = { change_development_level = 20 }
	1007.12.9 = {
		holder = araskaysit0001 #Dagan szel-Akalšes
	}
}

c_nasilan = {
	1000.1.1 = { change_development_level = 18 }
}

c_uqlum = {
	1000.1.1 = { change_development_level = 15 }
}

c_azkabar = {
	1000.1.1 = { change_development_level = 20 }
}

c_kalisad = {
	1000.1.1 = { change_development_level = 14 }
}

c_danas_aban = {
	1000.1.1 = { change_development_level = 16 }
}

c_mitiq = {
	1000.1.1 = { change_development_level = 17 }
}

c_orean = {
	1000.1.1 = { change_development_level = 15 }
}

c_setadazar = {
	1000.1.1 = { change_development_level = 19 }
}

c_markumar = {
	1000.1.1 = { change_development_level = 14 }
}

c_kuokrumar = {
	1000.1.1 = { change_development_level = 14 }
}